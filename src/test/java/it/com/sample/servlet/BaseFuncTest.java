package it.com.sample.servlet;

import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.stash.test.DefaultFuncTestData;
import com.atlassian.stash.test.runners.FuncTestSplitterRunner;
import com.atlassian.webdriver.stash.StashTestedProduct;
import com.atlassian.webdriver.stash.page.AboutPage;
import com.atlassian.webdriver.stash.page.StashLoginPage;
import com.atlassian.webdriver.stash.page.StashPage;
import com.atlassian.webdriver.testing.rule.SessionCleanupRule;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;

@RunWith(FuncTestSplitterRunner.class)
public class BaseFuncTest {

    protected final StashTestedProduct STASH = TestedProductFactory.create(StashTestedProduct.class);

    @Rule
    public WebDriverScreenshotRule screenshotRule = new WebDriverScreenshotRule();

    @Rule
    public SessionCleanupRule cleanupRule = new SessionCleanupRule();

    public void clearBrowserCookies() {
        STASH.getTester().getDriver().manage().deleteAllCookies();
    }

    protected <P extends StashPage> P loginAsAdmin(Class<P> page, Object... args) {
        return loginAs(DefaultFuncTestData.getAdminUser(), DefaultFuncTestData.getAdminPassword(), page, args);
    }

    protected <P extends StashPage> P loginAsRegularUser(Class<P> page) {
        return loginAs(DefaultFuncTestData.getRegularUser(), DefaultFuncTestData.getRegularUserPassword(), page);
    }

    protected <P extends StashPage> P loginAs(String username, String password, Class<P> page, Object... args) {
        clearBrowserCookies();
        return STASH.visit(StashLoginPage.class).login(username, password, page, args);
    }
    @Test
    public void gotToAbout() {
        AboutPage aboutPage = STASH.visit(AboutPage.class); //Should not need to log in
        assertTrue("About page should have copyright notice.", aboutPage.hasCopyrightNotice());
    }
}
